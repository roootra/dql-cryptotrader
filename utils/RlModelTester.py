import numpy
import stable_baselines3
import numpy as np
import pandas as pd
from stable_baselines3.common.evaluation import evaluate_policy
from env.AssetPortfolioLongEnv import AssetPortfolioLongEnv
from matplotlib import pyplot
from sklearn.preprocessing import MinMaxScaler, normalize
from sklearn.linear_model import LinearRegression
from typing import Any, Optional, Union
import talib


class RlModelTester(object):
    def __init__(self, price_data: pd.DataFrame, features_data: pd.DataFrame,
                 model: str, window: int = 1, model_algo: str = "TD3",
                 time_col: str = "Time", cash_in_weights=True):
        if price_data is None or features_data is None:
            raise ValueError("Price and/or features data are absent!")
        if price_data.shape[0] != features_data.shape[0]:
            raise AssertionError("Prices and features datasets have different number of rows!")
        if price_data.shape[0] <= window:
            raise AssertionError("There are less rows in price_data than observation window!")
        if model_algo not in ["DDPG", "SAC", "TD3"]:
            raise ValueError("model_algo should be one of the stable_baselines3 RL algorithms (currently supported "
                             "are DDPG, SAC, TD3)")
        self.num_assets = price_data.shape[1]
        self.price_data = price_data
        self.features_data = features_data
        self.time_col = time_col
        self.path_model = model
        self.model_algo = model_algo
        self.obs_window = window
        self.cash_in_weights = cash_in_weights
        self.weights = None
        # private methods
        self._load_model()

    def test(self, plot=True, benchmark_passive_port_weights: Union[None, list, tuple, np.ndarray] = None,
             calculate_portfolio_alpha: Union[None, np.ndarray, pd.DataFrame, pd.Series] = None,
             benchmark_portfolio_weights: Union[None, list, tuple, np.ndarray] = None):
        weights = self._calculate_weights()
        port_returns = self._calculate_port_returns()
        print("RL MODEL BENCHMARKING")
        print(f"Test data --- total obs.: {self.price_data.shape[0]}, number of quote assets: {self.num_assets}.")
        print(f"Avg. port. return is: {np.mean(port_returns)}.")
        print(f"Cumulative port. return is: {np.sum(port_returns)}.")
        print(f"Lowest portfolio drawdown is: {np.min(np.cumsum(port_returns))}.")
        benchmark = self._calculate_benchmark_port_returns(weights=benchmark_portfolio_weights)
        if plot:
            self._plot_portfolio_dynamics()
            self._plot_benchmark(benchmark_returns=benchmark)
        if calculate_portfolio_alpha is not None:
            raise NotImplementedError("TBD: User provides market benchmark, and CAPM hustle goes around here.")
            # Does CAPM work for crypto exchange market at all? What is return for a risk-free asset?
            ols = LinearRegression(fit_intercept=True, copy_X=False)  # is there r_f for market?
            ols.fit(X=calculate_portfolio_alpha, y=port_returns)
            print(f"Portfolio alpha + r_f is: {ols.intercept_}.")

    def _load_model(self):
        self.model = eval(f"stable_baselines3.{self.model_algo}.load('{self.path_model}')")

    def _calculate_weights(self, return_weights: bool = True):
        total_assets = self.num_assets + self.cash_in_weights
        weights = np.zeros(shape=(self.features_data.shape[0] + 1, total_assets))
        for i in range(self.obs_window, self.features_data.shape[0]):
            print(f"\r{i} out of {weights.shape[0]}", end="")
            current_obs = np.ravel(self.features_data.iloc[(i - self.obs_window):i, :])
            weights[i, :] = self.model.predict(current_obs, deterministic=True)[0] / 2 + 0.5
        print("")
        norm_weights = normalize(weights, norm="l1", axis=1)
        self.weights = norm_weights
        if return_weights:
            return self.weights

    def _calculate_port_returns(self, log=True, return_values=True) -> Any:
        self.returns = np.zeros(shape=(self.price_data.shape[0], self.price_data.shape[1]))
        self.portfolio_return = np.zeros(shape=self.returns.shape[0])
        if log:
            self.is_logreturn = True
            self.returns[1:, :] = np.log(self.price_data.loc[self.price_data.index, :]).diff().iloc[1:, :]
        else:
            self.is_logreturn = False
            self.returns[1:, :] = self.price_data.loc[self.price_data.index, :].iloc[1:, :] / \
                                  self.price_data.loc[self.price_data.index, :].iloc[:-1, :] - 1
        self.portfolio_return[1:] = np.sum(self.weights[1:-1, 1:] * self.returns[1:, :], axis=1)
        if return_values:
            return self.portfolio_return

    def _calculate_benchmark_port_returns(self, weights: Union[None, list, tuple, np.ndarray] = None) -> np.ndarray:
        if weights is None:
            print("Benchmark is a evenly weighted portfolio.")
            weights = np.ones(shape=self.num_assets) / self.num_assets
        if weights.shape[0] != self.num_assets:
            raise AssertionError(f"Number of weights ({weights.shape[0]}) should be equal to the number of quote "
                                 f"assets ({self.num_assets}).")
        benchmark_portfolio_return = np.tensordot(weights, self.returns, axes=(0, 1)).copy()
        print("###DEBUG###")
        print(benchmark_portfolio_return)
        return benchmark_portfolio_return

    def _plot_portfolio_dynamics(self):
        pyplot.rcParams.update({'font.size': 6})
        if self.weights is None:
            raise ValueError("Call calculate_weights before plotting portfolio weights.")
        else:
            img_nrows = self.num_assets + self.cash_in_weights + 1
            fig, axs = pyplot.subplots(nrows=img_nrows, sharex="row")
            fig.autofmt_xdate()
            sec_y_ax = list()
            axs[0].plot(self.portfolio_return, label="Portfolio return")
            sec_y_ax.append(axs[0].twinx())
            sec_y_ax[0].plot(np.cumsum(self.portfolio_return), color="green", label="Cumulative portfolio return")
            sec_y_ax[0].title.set_text("Portfolio performance")
            axs[0].legend()
            sec_y_ax[0].legend()
            axs[1].plot(self.weights[0], label="Base asset weight evolution")
            axs[1].legend()
            axs[1].title.set_text("Base asset weight evol")
            for i in range(1, self.weights.shape[1]):
                axs[i+1].plot(self.weights[:, i])
                axs[i+1].set_ylim([0, 1])
                if i != 0:
                    sec_y_ax.append(axs[i+1].twinx())
                    self.price_data.iloc[:, i - 1].plot(ax=sec_y_ax[i], secondary_y=True, color="green")
                    # binance_min_data_fd.iloc[:, i - 1].plot(ax=sec_y_ax[-1], secondary_y=True, color="green")
                    # returns[]
            pyplot.show()

    def _plot_benchmark(self, benchmark_returns: np.ndarray) -> None:
        fig, axs = pyplot.subplots(3)
        fig.autofmt_xdate()
        pyplot.title("Benchmarking")
        cumsum_portfolio = np.cumsum(self.portfolio_return)
        cumsum_benchmark = np.cumsum(benchmark_returns)
        axs[0].plot(cumsum_benchmark, color="blue", label="Cumulative benchmark portfolio returns")
        axs[0].plot(cumsum_portfolio, color="green", label="Cumulative active management portfolio "
                                                           "return")
        axs[0].legend()
        differential = self.portfolio_return - benchmark_returns
        axs[1].plot(differential, color="green", label="Returns' differential: active management premium")
        axs[1].legend()
        cum_differential = cumsum_portfolio - cumsum_benchmark
        axs[2].plot(cum_differential, label="Cumulative differential: active management performance.")
        axs[2].legend()
        pyplot.show()


if __name__ == "__main__":
    binance_futures_hourly_data = pd.read_csv("./data/binance_futures_hourly_3y.csv", index_col="Time").filter(
        like="_close")

    features_scaler = MinMaxScaler()
    features_scaler.fit(binance_futures_hourly_data)
    binance_h_norm = features_scaler.transform(binance_futures_hourly_data.iloc[:, :])

    rlModelTester = RlModelTester(price_data=binance_futures_hourly_data, features_data=binance_h_norm,
                                  model="./model/fitted_models/model_td3_hourly_prices_features_window.zip",
                                  window=1)
    rlModelTester.test()
