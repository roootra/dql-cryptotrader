import pandas as pd
import binance
import os

API_KEY = "urvvlZIV4EuYUsIgGk6gWkdSsOFXIYmJmcPjp5P2JmCQXHCbWZjUo5ZN4Rw1wvbK"
API_SECRET = "56j2j0sT8XquoYQy4OPNiZiXttMbeD0twhWJvliwk1MR0vON3QXGZUb0mJWhwi3r"


def get_data():
    api_key = globals()["API_KEY"]
    api_secret = globals()["API_SECRET"]
    print(os.getcwd())
    binance_client = binance.Client(api_key, api_secret)
    klines_columns = ["Open time", "Open", "High", "Low", "Close", "Volume", "Close time",
                      "Quote asset volume", "Number of trades", "Taker buy base asset volume",
                      "Taker buy quote asset volume", "Ignore"]
    fetch_symbols = ["BTCUSDT", "ETHUSDT", "LTCUSDT", "XMRUSDT", "DASHUSDT"]
    binance_data = pd.DataFrame([], columns=["Time"])
    for symbol in fetch_symbols:
        binance_current = \
            pd.DataFrame(binance_client.futures_historical_klines(symbol, binance.Client.KLINE_INTERVAL_1MINUTE,
                                                              "1 year ago UTC"),
                         columns=klines_columns)[["Close time", "Open", "High", "Low", "Close", "Volume"]]
        binance_current.columns = ["Time", symbol + "_open", symbol + "_high", symbol + "_low", symbol + "_close",
                                   symbol + "_volume"]
        binance_data = binance_data.merge(binance_current, on="Time", how="outer", )
    binance_data["Time"] = pd.to_datetime(binance_data["Time"], unit="ms")
    binance_data.iloc[:, 1:] = binance_data.iloc[:, 1:].astype("float")
    binance_data.to_csv("binance_futures_min_1y.csv", index=False)


if __name__ == "__main__":
    get_data()
