import numpy as np


class AssetPortfolio(object):
    def __init__(self, num_assets=1, init_asset=1, trans_cost=0.0001, hold_cost=0.0,
                 asset_names=None, even_weights_after_reset=False):
        self.num_assets = num_assets
        self.initial_base_asset_position = init_asset
        self.asset_names = asset_names
        # costs
        self.trans_cost = trans_cost
        self.hold_cost = hold_cost
        self.even_weights_after_reset = even_weights_after_reset  # are we initially in cash or have eq. weighted port.
        # to update portfolio returns
        self.reset()

    def calculate_return(self, last_prices, new_prices, new_weights, leverage=1, active_management_premium=False):
        # calculate returns first
        last_prices = np.append(1, last_prices)
        new_prices = np.append(1, new_prices)  # to count for cash
        new_returns = np.divide(new_prices, last_prices)
        # then adjust for transaction costs
        weights_diff = (new_returns * self.last_weights) / np.dot(new_returns, self.last_weights)
        trans_cost_burden = self.trans_cost * np.abs(weights_diff[1:] - new_weights[1:]).sum()
        # and calculate sum of weighted returns
        current_port_return = ((np.dot(new_returns, new_weights) - 1) * leverage + 1) # cash has return 1
        # finally, estimate current portfolio value
        new_portfolio_val = self.last_portfolio_val * current_port_return *\
                            (1 - trans_cost_burden) * (1 - self.hold_cost)
        # check whether bankrupt (can't go bankrupt for leverage = 1), assuming cross-margin
        bankrupt = bool(np.less_equal(current_port_return, 0))
        # variables to return
        port_return = (new_portfolio_val / self.last_portfolio_val) - 1
        # update portfolio val and weights
        self.last_weights = new_weights
        self.last_portfolio_val = new_portfolio_val
        # step info
        info = {
            "Portfolio gross return": port_return + 1,
            "Portfolio net return": port_return,
            "Portfolio net return with leverage": port_return * leverage,
            "Portfolio value": new_portfolio_val,
            "New weights": new_weights
        }
        self.info_list.append(info)
        if active_management_premium:
            return (port_return - np.mean(new_returns[1:] - 1))*leverage, info, bankrupt
        else:
            return port_return * leverage, info, bankrupt

    def reset(self):
        self.info_list = list()
        self.last_portfolio_val = self.initial_base_asset_position
        if self.even_weights_after_reset:
            self.last_weights = np.ones(shape=self.num_assets + 1, dtype=np.float32) / (self.num_assets + 1)
        else:
            self.last_weights = np.append(1.0, np.zeros(shape=self.num_assets, dtype=np.float32))
