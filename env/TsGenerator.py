import numpy as np
import pandas as pd


class TsGenerator(object):
    def __init__(self, price_data: pd.DataFrame, features_data: pd.DataFrame,
                 sample_periods: int = 100, window: int = 24, random_stop=False, min_random_stop=1):
        """
        https://arxiv.org/abs/1712.00378

        :param price_data: TBD
        :param features_data: TBD
        :param sample_periods: TBD
        :param window: TBD
        :param random_stop: TBD
        :param min_random_stop: TBD
        """
        if price_data is None or features_data is None:
            raise ValueError("Prices and/or features data are absent!")
        if price_data.shape[0] != features_data.shape[0]:
            raise AssertionError("Prices and features datasets have different number of rows!")
        if price_data.shape[0] <= window:
            raise AssertionError("There are less rows in price_data than observation window!")
        self.asset_names = price_data.columns
        self.sample_periods = sample_periods
        self.window = window
        self.price_data = price_data.to_numpy()
        self.features_data = features_data.to_numpy()
        self.data_length = self.price_data.shape[0]
        self.random_stop = random_stop
        self.min_random_stop = min_random_stop
        self.current_random_stop_period = 0
        print(f"There are {self.data_length} observations of {len(self.asset_names)} quote assets. "
              f"{self.features_data.shape[1]} features.")
        self.reset()

    def get_current_obs(self):
        current_prices_window = \
            self.price_data[(self.current_period - 1):(self.current_period + 1), :].copy()  # Fancy indexing
        current_features_window = \
            self.features_data[(self.current_period - self.window + 1):(self.current_period + 1), :].copy()
        if np.equal(current_prices_window, 0).any():  # debug
            breakpoint()
        if np.isnan(current_prices_window).any():  # debug
            breakpoint()
        self.current_period += 1
        self.steps_passed += 1
        if self.random_stop:
            is_done = self.steps_passed > self.current_random_stop_period
        else:
            is_done = self.steps_passed > self.sample_periods
        return current_prices_window, current_features_window, is_done

    def get_initial_prices(self):
        return self.price_data[self.initial_period, :]

    def reset(self):
        self.steps_passed = 0
        self.initial_period = np.random.randint(low=self.window, high=self.data_length - self.sample_periods)
        self.current_period = self.initial_period
        if self.random_stop:
            self.current_random_stop_period = np.random.randint(low=self.min_random_stop, high=self.sample_periods)


