import numpy as np
import pandas as pd
import matplotlib.pyplot as pyplot
import gym
from env.TsGenerator import TsGenerator
from env.AssetPortfolio import AssetPortfolio


class AssetPortfolioLongEnv(gym.Env):
    def __init__(self, price_data: pd.DataFrame, features_data: pd.DataFrame, steps: int = 100,
                 obs_window: int = 100, trans_cost: float = 0.0001,
                 hold_cost: float = 0, random_stop=False, min_random_stop=1,
                 leverage: float = 1, init_asset: float = 1, obs_lower=0, obs_upper=1,
                 active_management_premium=False, start_from_even_weights=False):
        if price_data is None or features_data is None:
            raise ValueError("Price and/or features data are absent!")
        if price_data.shape[0] != features_data.shape[0]:
            raise AssertionError("Prices and features datasets have different number of rows! "
                                 f"({price_data.shape[0]} vs. {features_data.shape[0]}).")
        self.number_assets = price_data.shape[1]
        self.obs_window = obs_window
        self.trans_cost = trans_cost
        self.hold_cost = hold_cost
        self.steps = steps
        self.asset_names = price_data.columns
        self.num_features = features_data.shape[1]
        self.leverage = leverage
        self.active_management_premium = active_management_premium
        self.start_from_even_weights = start_from_even_weights
        self.random_stop = random_stop
        self.min_random_stop = min_random_stop
        self.data_generator = TsGenerator(price_data=price_data, features_data=features_data,
                                          sample_periods=steps, window=obs_window + 1,
                                          random_stop=random_stop, min_random_stop=min_random_stop)
        self.dates = price_data.index
        self.trans_cost = trans_cost
        self.info_list = list()
        self.portfolio = AssetPortfolio(num_assets=self.number_assets, init_asset=init_asset,
                                        trans_cost=self.trans_cost, hold_cost=self.hold_cost,
                                        asset_names=self.asset_names,
                                        even_weights_after_reset=self.start_from_even_weights)
        # with cash, following stable-baselines3 recommendations about action space in [-1,1]^num_assets
        self.action_space = gym.spaces.Box(-1, 1, shape=(self.number_assets + 1,), dtype=np.float32)
        print(f"Action space shape (number of portfolio weights incl. cash) is {self.action_space.shape}.")
        obs_shape = (self.num_features*self.obs_window,)
        self.observation_space = gym.spaces.Box(obs_lower, obs_upper, shape=obs_shape, dtype=np.float32)
        print(f"Observation space shape (number of features*window) is {self.observation_space.shape}")

    def step(self, action):
        return self._step(action)

    def reset(self):
        return self._reset()

    def render(self, mode='human', close=False):
        return self._render(mode, close=False)

    def plot(self):
        return self._plot()

    def _step(self, action):
        # Recover weights: [-1, 1] -> [0, 1]
        action = (action / 2.0) + 0.5
        # Normalize weights |action| == 1
        action = action / np.sum(action)
        with np.printoptions(precision=3, suppress=True):
            print(action)  # debug
        # Get the data
        current_prices_window, current_features_window, is_done = self.data_generator.get_current_obs()
        current_prices, last_prices = current_prices_window[-1, :], current_prices_window[-2, :]
        # Calculate new return of portfolio
        port_return, info, is_bankrupt = self.portfolio.calculate_return(last_prices, current_prices, action,
                                                                         self.leverage, self.active_management_premium)
        self.info_list.append(info)
        # return state, reward, is_done or is_done, info
        return current_features_window[1:, :].flatten(), port_return, is_done or is_bankrupt, info

    def _reset(self):
        self.info_list = list()
        self.data_generator.reset()
        self.portfolio.reset()
        current_prices_window, current_features_window, is_done = self.data_generator.get_current_obs()
        return current_features_window[1:, :].flatten()

    def _render(self, mode='human', close=False):
        if close:
            return
        if mode == 'ansi':
            if len(self.info_list) == 0:
                print("No steps are fulfilled! Nothing to render.")
                return -1
            else:
                print(self.info_list[-1])
        elif mode == 'human':
            self.plot()

    def _plot(self):
        if len(self.info_list) == 0:
            print("No steps are fulfilled! Nothing to plot.")
            return -1
        else:
            pd_info = pd.DataFrame(self.info_list)
            fig, (ax1, ax2) = pyplot.subplots(2)
            # pyplot.xticks(range(self.steps), pd_info.index, rotation=45)
            ax1.plot(pd_info['Portfolio value'], label='Portfolio value', color="blue")
            ax1.axhline(y=1, color='black', linestyle='dashed', markersize=0.1)
            ax2.plot(pd_info['Portfolio gross return'], label='Portfolio gross return', color="red")
            ax2.axhline(y=1, color='black', linestyle='dashed', markersize=0.1)
            fig.legend()
            fig.suptitle("Portfolio statistics")
            fig.show()
