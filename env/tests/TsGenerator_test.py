import unittest

import numpy as np
import pandas as pd
import os
from env.TsGenerator import TsGenerator

FirstTestIntitalPrice = 0

class TsGeneratorTest(unittest.TestCase):
    def setUp(self):
        self.binance_hourly = pd.read_csv("data/binance_hourly_3y.csv").drop("Time", axis=1)
        self.test_tsGenerator = TsGenerator(self.binance_hourly, self.binance_hourly)

    def test_getInitialPrices(self):
        print(f"Working directory is {os.getcwd()}.")
        print("Testing main logic...")
        a = self.test_tsGenerator.get_initial_prices()
        print(a)
        global FirstTestIntitalPrice
        FirstTestIntitalPrice = a
        aa = self.test_tsGenerator.get_current_obs()[0][-1]
        print(aa)
        b = self.test_tsGenerator.get_current_obs()[0][-1]
        print(b)
        aaa = self.test_tsGenerator.get_initial_prices()
        print(aaa)
        np.testing.assert_array_equal(a, aa, "First initial price is {}, first current obs is {}.".format(a, aa))
        np.testing.assert_array_equal(a, aaa, "First initial price is {}, second initial price is {}.".format(a, aaa))
        np.testing.assert_array_equal(aa, aaa, "First current obs is {}, second initial price is {}.".format(aa, aaa))

    def test_reset(self):
        print("Testing reset function...")
        global FirstTestIntitalPrice
        self.test_tsGenerator.reset()
        print("Reset has been triggered!")
        a = self.test_tsGenerator.get_initial_prices()
        aa = self.test_tsGenerator.get_current_obs()[0][-1]
        b = self.test_tsGenerator.get_current_obs()[0][-1]
        aaa = self.test_tsGenerator.get_initial_prices()
        np.testing.assert_array_equal(a, aa, "First initial price is {}, first current obs is {}.".format(a, aa))
        np.testing.assert_array_equal(a, aaa, "First initial price is {}, second initial price is {}.".format(a, aaa))
        np.testing.assert_array_equal(aa, aaa, "First current obs is {}, second initial price is {}.".format(aa, aaa))
        self.assertFalse(np.array_equal(FirstTestIntitalPrice, a),\
                         "Initial price before reset was {}, after reset is {}".format(FirstTestIntitalPrice, a))

    def test_random_stop(self):
        np.random.seed(123)
        print("Testing random stop option...")
        self.test_tsGenerator = TsGenerator(self.binance_hourly, self.binance_hourly,
                                            random_stop=True, min_random_stop=1)
        total_steps_expected = list()
        total_steps_done = list()
        times = 10
        for i in range(times):
            self.test_tsGenerator.reset()
            total_steps_expected.append(self.test_tsGenerator.current_random_stop_period)
            for j in range(self.test_tsGenerator.sample_periods):
                _, _, is_done = self.test_tsGenerator.get_current_obs()
                if is_done:
                    total_steps_done.append(j)
                    break
        print(f"Total steps expected: {total_steps_expected}.")
        print(f"Total steps done: {total_steps_done}.")
        np.testing.assert_equal(len(total_steps_done), times, "There are less values in total steps done list.")
        np.testing.assert_equal(len(total_steps_done), times, "There are less values in total steps done list.")
        np.testing.assert_equal(len(set(total_steps_expected)) > 1, True,
                                "Number of elements among all resets are equal!")
        np.testing.assert_array_equal(total_steps_done, total_steps_expected, "Numbers of expected and done steps "
                                                                              "differ!")


if __name__ == '__main__':
    unittest.main()
