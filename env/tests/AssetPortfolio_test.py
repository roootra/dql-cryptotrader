import unittest
from env.AssetPortfolio import AssetPortfolio

FirstTestInitialPrice = 0


class AssetPortfolioTest(unittest.TestCase):
    def setUp(self):
        self.test_assetPortfolio = AssetPortfolio(num_assets=2, asset_names=["Test1", "Test2"])

    def test_fiftyfifty(self):
        print("First asset falls by 50%, second asset rises by 50%")
        ret = self.test_assetPortfolio.calculate_return(last_prices=[1, 1], new_prices=[0.5, 1.5],
                                             new_weights=[1 / 3, 1 / 3, 1 / 3])
        self.assertAlmostEqual(-6.67e-05, ret[0], 2, "First return is wrong.")
        self.assertFalse(ret[2], "First operation has wrongly led to bankruptcy.")

    def test_bankruptcy(self):
        print("Simulating bankruptcy...")
        ret = self.test_assetPortfolio.calculate_return(last_prices=[0.5, 1.5], new_prices=[0, 0],
                                                   new_weights=[0, 1 / 2, 1 / 2])
        self.assertEqual(-1.0, ret[0], "Second return is not full portfolio loss.")
        self.assertTrue(ret[2], "Second operation hasn't led to bankruptcy.")

    def test_reset(self):
        self.test_assetPortfolio.reset()
        print("Reset has been triggered!")
        print("After reset, first asset falls by 50%, second asset rises by 50%")
        ret = self.test_assetPortfolio.calculate_return(last_prices=[1, 1], new_prices=[0.5, 1.5],
                                                        new_weights=[1 / 3, 1 / 3, 1 / 3])
        self.assertAlmostEqual(-6.67e-05, ret[0], 2, "First return is wrong after reset.")
        self.assertFalse(ret[2], "First operation has wrongly led to bankruptcy after reset.")


if __name__ == '__main__':
    unittest.main()
