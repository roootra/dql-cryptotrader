import unittest
import numpy as np
import pandas as pd
from env.AssetPortfolioLongEnv import AssetPortfolioLongEnv


class MyTestCase(unittest.TestCase):
    def setUp(self):
        binance_h = pd.read_csv("data/binance_hourly_3y.csv")
        binance_h.set_index("Time", inplace=True)
        binance_h = binance_h.filter(like="_close", axis=1).iloc[:, :3]
        print(binance_h.head())
        self.test_env_Portfolio_long = AssetPortfolioLongEnv(binance_h, binance_h)

    def test_make_steps(self):
        a = self.test_env_Portfolio_long.step(np.array(([1, -1, -1, -1])))
        print(a[1:5])
        self.assertEqual(a[1], 0, "Initial portfolio return is not zero!")
        self.assertFalse(a[2], "Initial portfolio is immediately bankrupt!")
        b = self.test_env_Portfolio_long.step(np.array([-0.5, -0.5, -0.5, -0.5]))
        c = self.test_env_Portfolio_long.step(np.array([-1, 1, -1, -1]))
        d = self.test_env_Portfolio_long.step(np.array([-1, -1, 1, -1]))
        e = self.test_env_Portfolio_long.step(np.array([-1, -1, -1, 1]))
        f = self.test_env_Portfolio_long.step(np.array([-1, 0, -1, 0]))
        print(f[1:4])
        #self.assertLess(0, f[1], "Portfolio gross return is lower than zero after steps!")

    def test_representation(self):
        self.test_env_Portfolio_long.step(np.array(([1, -1, -1, -1])))
        self.test_env_Portfolio_long.step(np.array([-0.5, -0.5, -0.5, -0.5]))
        self.test_env_Portfolio_long.step(np.array([-1, 1, -1, -1]))
        self.test_env_Portfolio_long.step(np.array([-1, -1, 1, -1]))
        self.test_env_Portfolio_long.step(np.array([-1, -1, -1, 1]))
        self.test_env_Portfolio_long.step(np.array([-1, 0, -1, 0]))
        a = self.test_env_Portfolio_long.plot()
        b = self.test_env_Portfolio_long.render(mode='ansi')
        self.assertIsNone(a, "Plot is not empty before reset!")
        self.assertIsNone(b, "Render is not empty before reset!")
        self.test_env_Portfolio_long.reset()
        aa = self.test_env_Portfolio_long.plot()
        bb = self.test_env_Portfolio_long.render(mode="ansi")
        print(b)
        print(bb)
        self.assertEqual(aa, -1, "Plot is not declined after reset!")
        self.assertEqual(bb, -1, "Render is not declined after reset!")


if __name__ == '__main__':
    unittest.main()
