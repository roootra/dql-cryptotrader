import os

import binance
import numpy as np
import pandas as pd
import stable_baselines3
import sys
import json
import sqlite3
import itertools
from multiprocessing.dummy import Pool

API_KEY = "ENTER YOUR BINANCE API KEY HERE"
API_SECRET = "ENTER YOUR BINANCE API SECRET HERE"
PHRASE_CREATE_SQLITE_DB = '''CREATE TABLE prices 
(close_time INTEGER NOT NULL, 
symbol TEXT NOT NULL, 
open REAL, 
high REAL, 
low REAL, 
close REAL, 
volume REAL,
PRIMARY KEY (close_time, symbol));'''
# ANSI colors
ANSICOL = [
    "\033[0m",   # End of color
    "\033[32m",  # Green
    "\033[91m",  # Red
    "\033[93m",  # Yellow
    "\033[34m",  # Blue
    "\033[36m",  # Cyan
    "\033[35m",  # Magenta
]


class BinanceTrader(object):
    def __init__(self, binance_key: str, binance_secret: str, testnet=False):
        self.client = binance.Client(api_key=binance_key, api_secret=binance_secret, testnet=testnet)
        print(ANSICOL[4] + "Client: Binance server time is {}.".format(self.client.get_server_time()["serverTime"]) +
              ANSICOL[0])
        self.websocket = binance.ThreadedWebsocketManager(api_key=binance_key, api_secret=binance_secret,
                                                          testnet=testnet)
        print(ANSICOL[1] + "Websocket has been initialized..." + ANSICOL[0])
        self.sqldb = sqlite3.connect(":memory:", check_same_thread=False)
        self.sqldb_cursor = self.sqldb.cursor()
        self.sqldb_cursor.execute(PHRASE_CREATE_SQLITE_DB)
        self.sqldb.commit()
        print(ANSICOL[1] + "Sqlite3 db has been initialized in RAM..." + ANSICOL[0])
        self.num_assets = None
        self.current_data = None
        self.symbols = None
        self.streams = None
        self.candles_updated = 0
        self.model = None
        self.historical_data_ready = False

    def load_model(self, model: str, feature_window: int = 1, algo: str = "TD3"):
        if algo not in ["TD3", "DDPG", "A2C", "DQN", "HER", "PPQ", "SAC"]:
            raise ValueError("Wrong algorithm is specified! Provide a correct algorithm")
        self.model = eval(f"stable_baselines3.{algo}.load('{model}')")
        self.feature_window = feature_window
        print(ANSICOL[1] + "Model is loaded!" + ANSICOL[0])

    @staticmethod
    def create_stream_names(symbols: list, contract_type="perpetual", interval="1h"):
        if contract_type == "spot":
            streams = [symbol.lower() + "@kline_" + interval for symbol in symbols]
        elif contract_type == "perpetual":
            streams = [symbol.lower() + "_perpetual@continuousKline_" + interval for symbol in symbols]
        elif contract_type == "current_quarter":
            streams = [symbol.lower() + "_current_quarter@continuousKline_" + interval for symbol in symbols]
        elif contract_type == "next_quarter":
            streams = [symbol.lower() + "_next_quarter@continuousKline_" + interval for symbol in symbols]
        else:
            raise ValueError("Wrong contract type! Choose either spot or perpetual one.")
        return streams

    def start_trading(self, symbols: list, contract_type="perpetual", interval="1h", window="48 hours ago UTC"):
        if self.model is None:
            raise ValueError("Model is not loaded! Use .model method to specify model before trading.")
        self.symbols = symbols
        self.streams = self.create_stream_names(self.symbols, contract_type=contract_type, interval=interval)
        self.num_assets = len(symbols)
        self.websocket.start()
        self.websocket.start_futures_multiplex_socket(callback=self.handle_message, streams=self.streams)
        print(ANSICOL[1] + "Websocket has been subscribed to streams." + ANSICOL[0])
        self.get_historical_data(symbols=symbols, window=window,
                                 interval=interval, contract_type=contract_type)

    def handle_message(self, msg, verbose=False):
        if verbose:
            print(msg)
        is_kline_done = msg["data"]["k"]["x"]
        stream, symbol = msg["stream"], msg["data"]["ps"]
        timeo, timec = int(msg["data"]["k"]["t"]), int(msg["data"]["k"]["T"])
        o, h, l, c, vol = float(msg["data"]["k"]["o"]), \
                          float(msg["data"]["k"]["h"]), \
                          float(msg["data"]["k"]["l"]), \
                          float(msg["data"]["k"]["c"]), \
                          float(msg["data"]["k"]["v"])
        # is candle in current_data outdated (is last timeC < current timeO)?
        if is_kline_done:  # if yes, put it into db
            self.sqldb_cursor.execute("INSERT INTO prices (close_time, symbol, open, high, low, close, volume) "
                                      "VALUES (?, ?, ?, ?, ?, ?, ?) "
                                      "ON CONFLICT (close_time, symbol) DO UPDATE SET "
                                      "open=excluded.open, high=excluded.high, low=excluded.low, close=excluded.close, "
                                      "volume=excluded.volume;",
                                      [timec, symbol, o, h, l, c, vol])
            self.candles_updated += 1
            if self.candles_updated == self.num_assets:
                self.candles_updated = 0
                self.make_decision()

    def make_decision(self):
        # I need define close time manually
        # self.feature_window
        last_candles = list()
        for symbol in self.symbols:
            last_candles.append(self.sqldb_cursor.execute(
                "SELECT * FROM prices WHERE symbol = ? ORDER BY close_time DESC LIMIT ?",
                [symbol, self.feature_window]).fetchall())
        last_data = list(itertools.chain.from_iterable(last_candles))
        pd_long = pd.DataFrame(last_data,
                               columns=["close_time", "symbol", "open", "high", "low", "close", "vol"])
        pd_long["close_time"] = pd.to_datetime(pd_long["close_time"], unit="ms")
        pd_wide = pd_long.pivot(index="close_time", columns="symbol")
        print("pd_wide_loc")
        print(pd_wide.loc[:, (slice(None), self.symbols)])
        if self.historical_data_ready:
            data_to_model = np.ravel(pd_wide.loc[::-1, (slice(None), self.symbols)])
            new_weights = self.model.predict(data_to_model)[0] / 2 + 0.5
            print(new_weights)
            test_futures_account = self.client.futures_account()
            base_asset_position = dict()
            for item in test_futures_account['assets']:
                base_asset_position[item['asset']] = item
            current_positions = dict()
            for item in test_futures_account['positions']:
                current_positions[item['symbol']] = item

            #for weight in new_weights:
            #    pass

    def get_historical_data(self, symbols: list, window="48 hours ago UTC",
                            interval="1h", contract_type="perpetual"):
        # TODO: It might be that websocket has put recent complete data into DB, and the most recent row of REST is
        #  outdated => using SQL syntax insert without updating rows
        print(ANSICOL[3] + "Started fetching historical data using REST API..." + ANSICOL[0])
        klines_columns = ["open_time", "open", "high", "low", "close", "volume", "close_time",
                          "quote_asset_volume", "number_of_trades", "taker_buy_base_asset_volume",
                          "taker_buy_quote_asset_volume", "ignore"]
        if contract_type == "perpetual":
            getter = self.client.futures_historical_klines
        elif contract_type == "spot":
            getter = self.client.get_historical_klines
        else:
            raise ValueError("Wrong contract type! Choose either spot or perpetual one.")
        for symbol in symbols:
            binance_current = \
                pd.DataFrame(getter(symbol=symbol, interval=interval, start_str=window),
                             columns=klines_columns)[["close_time", "open", "high", "low", "close", "volume"]]
            binance_current["symbol"] = symbol
            for index, row in binance_current.iterrows():
                self.sqldb_cursor.execute("INSERT OR IGNORE INTO prices (close_time, symbol, open, high, low, close, "
                                          "volume) VALUES (?, ?, ?, ?, ?, ?, ?);",
                                          [row["close_time"], row["symbol"], row["open"], row["high"],
                                           row["low"], row["close"], row["volume"]])
        #self.sqldb.commit()
        self.historical_data_ready = True
        print(ANSICOL[1] + "Historical data are added to DB!" + ANSICOL[0])



if __name__ == "__main__":
    symbols = ["BTCUSDT", "ETHUSDT", "LTCUSDT", "XMRUSDT", "DASHUSDT"]
    binanceTrader = BinanceTrader(binance_key=globals()["API_KEY"],
                                  binance_secret=globals()["API_SECRET"], testnet=False)
    binanceTrader.load_model("model/fitted_models/model_good_1/TD3.zip", feature_window=48)
    binanceTrader.start_trading(symbols, interval="1m")
