import os

import numpy as np

from env.AssetPortfolioLongEnv import *
import stable_baselines3
from stable_baselines3.common.env_checker import check_env
from sb3_contrib.common.wrappers.time_feature import TimeFeatureWrapper
from stable_baselines3.common.noise import OrnsteinUhlenbeckActionNoise, NormalActionNoise
from stable_baselines3.common.logger import Figure as sb3_log_Figure, configure as sb3_log_configure
from model.etc.learning_rate import linear_schedule
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import TimeSeriesSplit
import talib
from utils.RlModelTester import RlModelTester
import matplotlib.pyplot as plt
import torch

# TODO: Features only at time t are needed, not for the whole obs window.
# TODO: News???
# TODO: Utility function?
# TODO: GAN for ts --- juster.
def main():
    print("Working directory:")
    print(os.getcwd())

    torch.autograd.set_detect_anomaly(True)

    window = 24*2
    learning_rate = 1e-4
    n_steps = 24*5
    batch_size = 24*5

    test_ratio = 0.1

    binance_h = pd.read_csv("data/binance_futures_hourly_3y.csv", index_col="Time")
    binance_h.dropna(inplace=True)
    binance_h_ohlc = binance_h #OHLC
    binance_h = binance_h.filter(like="_close", axis=1)

    binance_h_features = generate_features(binance_h)
    binance_h_features.dropna(inplace=True)
    complete_ind = binance_h_features.index
    binance_h = binance_h.iloc[complete_ind, :]
    print(binance_h)

    #SCALER
    close_data_scaler = MinMaxScaler()
    close_data_scaler.fit(binance_h)
    binance_h_scaled = pd.DataFrame(close_data_scaler.transform(binance_h.iloc[:, :]), columns=binance_h.columns)

    features_scaler = MinMaxScaler()
    features_scaler.fit(binance_h_features)
    binance_h_features_scaled = pd.DataFrame(features_scaler.transform(binance_h_features.iloc[:, :]),
                                             columns=binance_h_features.columns)

    ohlc_data_scaler = MinMaxScaler()
    ohlc_data_scaler.fit(binance_h_ohlc)
    binance_h_ohlc_scaled = pd.DataFrame(ohlc_data_scaler.transform(binance_h_ohlc), columns=binance_h_ohlc.columns)

    ts_train_index = int(binance_h_ohlc_scaled.shape[0] * (1 - test_ratio))

    policy_kwargs = {"net_arch": [1024, 512, 256, 128]}

    env = AssetPortfolioLongEnv(price_data=binance_h_ohlc.filter(like="_close", axis=1).iloc[:ts_train_index,:],
                                features_data=binance_h_ohlc_scaled.iloc[:ts_train_index,:],
                                steps=n_steps, obs_window=window,
                                trans_cost=0.0004, leverage=1, obs_lower=0, obs_upper=1,
                                active_management_premium=True,
                                random_stop=True, min_random_stop=24*1)
    check_env(env)
    # Noises
    mr_noise = OrnsteinUhlenbeckActionNoise(mean=np.zeros(binance_h.shape[1] + 1),
                                         sigma=np.ones(binance_h.shape[1] + 1)*0.001)
    g_noise = NormalActionNoise(mean=np.zeros(binance_h.shape[1] + 1),
                                         sigma=np.ones(binance_h.shape[1] + 1)*0.005)

    model = stable_baselines3.TD3("MlpPolicy", env,
                                  batch_size=batch_size,
                                  verbose=1,
                                  action_noise=g_noise,
                                  policy_kwargs=policy_kwargs,
                                  tensorboard_log="./tensorboard_log/",
                                  create_eval_env=True,
                                  learning_rate=linear_schedule(1e-4 * 0.7))
                                  #learning_rate=learning_rate)
    model.learn(total_timesteps=int(1e5))
    model.save("model/fitted_models/model_td3_hourly_prices_features_window")

    rl_model_tester = RlModelTester(price_data=binance_h_ohlc.filter(like="_close", axis=1).iloc[ts_train_index:,:],
                                    features_data=binance_h_ohlc_scaled.iloc[ts_train_index:,:],
                                    model="./model/fitted_models/model_td3_hourly_prices_features_window.zip",
                                    window=window)
    rl_model_tester.test()


def generate_features(pd_ts, series=None):
    features = pd.DataFrame(index=range(pd_ts.shape[0]))
    for serie in pd_ts.columns:  # add moving variance
        current_features = pd.DataFrame({
            serie + "_ma3": talib.MA(pd_ts[serie].to_numpy(), timeperiod=3),
            serie + "_ma6": talib.MA(pd_ts[serie].to_numpy(), timeperiod=6),
            serie + "_ma12": talib.MA(pd_ts[serie].to_numpy(), timeperiod=12),
            serie + "_ma24": talib.MA(pd_ts[serie].to_numpy(), timeperiod=24),
            serie + "_ma48": talib.MA(pd_ts[serie].to_numpy(), timeperiod=48),
            serie + "_ema3": talib.EMA(pd_ts[serie].to_numpy(), timeperiod=3),
            serie + "_ema6": talib.EMA(pd_ts[serie].to_numpy(), timeperiod=6),
            serie + "_ema12": talib.EMA(pd_ts[serie].to_numpy(), timeperiod=12),
            serie + "_ema24": talib.EMA(pd_ts[serie].to_numpy(), timeperiod=24),
            serie + "_ema48": talib.EMA(pd_ts[serie].to_numpy(), timeperiod=48),
            serie + "_momentum1": talib.MOM(pd_ts[serie].to_numpy(), 1),
            serie + "_momentum3": talib.MOM(pd_ts[serie].to_numpy(), 6),
            serie + "_momentum6": talib.MOM(pd_ts[serie].to_numpy(), 12),
            serie + "_momentum12": talib.MOM(pd_ts[serie].to_numpy(), 24),
            serie + "_rsi6": talib.RSI(pd_ts[serie].to_numpy(), 6),
            serie + "_rsi12": talib.RSI(pd_ts[serie].to_numpy(), 12),
            serie + "_rsi24": talib.RSI(pd_ts[serie].to_numpy(), 24),
            serie + "_rsi48": talib.RSI(pd_ts[serie].to_numpy(), 48),
            serie + "_std48": pd_ts[serie].rolling(48).std().to_numpy(),
            #serie + "_arimaf": ARIMA(pd_ts[serie].to_numpy(), order=[1, 1, 1]).fit().fittedvalues
        })
        if series is not None:
            current_features = pd.concat([current_features, series], axis=1, ignore_index=True)
        features = pd.concat([features, current_features], axis=1, ignore_index=True)
    return features


if __name__ == '__main__':
    main()