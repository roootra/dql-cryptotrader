from sklearn.preprocessing import minmax_scale, scale
from typing import Optional
import pandas as pd
import numpy as np


# What to do with scaling metadata???
def prepare_data(price_data: pd.DataFrame,
                 features_data: Optional[pd.DataFrame],
                 scale_features: Optional[str] = "MinMaxScaler",
                 **kwargs) -> (pd.DataFrame, pd.DataFrame):
    if scale_features not in ("minmax", "standardize", "gr_rates", "rates", "log_rates") or not None:
        raise ValueError('scale_features should be either None, "minmax", "standardize", '
                         '"gr_rates", "rates or ""log_rates".')
    price_data.dropna(inplace=True)

    if scale_features == "rates":
        features_data = features_data.pct_change()
        features_data.dropna(inplace=True)
    elif scale_features == "gr_rates":
        features_data = features_data.pct_change() + 1
        features_data.dropna(inplace=True)
    elif scale_features == "log_rates":
        log_features_data = features_data.copy()
        log_features_data.iloc[:, :] = np.log(features_data)
        features_data = log_features_data.diff()
        del log_features_data
        features_data.dropna(inplace=True)
    else:
        features_data.dropna(inplace=True)
        set_price_index, set_features_index = set(price_data.index), set(features_data.index)
        intersect_index = set_price_index.intersection(set_features_index)
        features_data = features_data.loc[intersect_index, :]
        if scale_features == "minmax":
            features_data.iloc[:, :] = minmax_scale(features_data)
        elif scale_features == "standardize":
            features_data.iloc[:, :] = scale(features_data)

